'use strict'

class SiteController {
  async index() {
    return {
      name: 'Adonis Admin',
      menu: [
        {
          "name": "Home",
          "url": "/",
          "icon": "fa fa-home"
          // for home page
        },
        {
          "name": "Content",
          "title": true
          // display as a delimiter
        },
        {
          "name": "Users",
          "url": "/rest/users",
          "icon": "fa fa-users"
          // url format of resource list: /rest/:resourceName
        },
        {
          "name": "Posts",
          "url": "/rest/posts",
          "icon": "fa fa-list"
          // url format of resource list: /rest/:resourceName
        },
      ]
    }
  }
}

module.exports = SiteController
